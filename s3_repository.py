import boto3
import botocore.config
import jsonpickle
from typing import Union

from config import *
from model.block_info import BlockInfo
from util import retry


class S3Repository(object):
    __s3 = boto3.resource('s3',
                          region_name=AMAZON_REGION,
                          aws_access_key_id=AMAZON_ID,
                          aws_secret_access_key=AMAZON_KEY,
                          config=botocore.config.Config(max_pool_connections=100))

    @retry(retries=5)
    def get_block_info(self, block_num: int) -> Union[None, BlockInfo]:
        obj = self.__s3.Object(BLOCK_BUCKET, self._get_file_name(block_num))
        loaded = obj.get()['Body'].read()
        return jsonpickle.loads(loaded)

    def save_block_info(self, block_num: int, block_info: BlockInfo):
        self.__s3.Object(BLOCK_BUCKET, self._get_file_name(block_num)).put(Body=jsonpickle.dumps(block_info))

    def _get_file_name(self, block_num: int):
        return str(block_num) + '.json'
