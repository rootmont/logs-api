import os

AMAZON_ID = os.environ.get('AMAZON_ID', '')
AMAZON_KEY = os.environ.get('AMAZON_KEY', '')
AMAZON_REGION = "us-west-1"
BLOCK_BUCKET = "eth-block-store"

