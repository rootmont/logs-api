import json
import logging

from flask import Flask, Response
from s3_repository import S3Repository

app = Flask(__name__)


@app.route('/logs-api/block/<int:block_number>',  methods=['GET'], strict_slashes=False)
def get_block_path(block_number):
    return get_block(block_number)


@app.route('/block/<int:block_number>',  methods=['GET'], strict_slashes=False)
def get_block(block_number):
    if block_number is None:
        logging.info("blockNumber was None")
        return Response("{}", status=400, mimetype='application/json')
    repo = S3Repository()
    block = repo.get_block_info(block_number)
    return Response(json.dumps(block.to_json()), status=200, mimetype='application/json')


@app.route('/health',  methods=['GET'])
def health():
    return Response("{}", status=200, mimetype='application/json')


if __name__ == '__main__':
    app.debug = False
    app.run(host="0.0.0.0", port=int(5000))
