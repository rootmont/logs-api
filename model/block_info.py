from typing import List, Dict
import json
import decimal


class LogsInfo(object):
    def __init__(self, address: str, topics: List[str], data: str):
        self.address: str = address
        self.topics: List[str] = topics
        self.data: str = data

    def to_json(self) -> Dict:
        return {
            'address': self.address,
            'topics': self.topics,
            'data': self.data
        }


class BlockInfo(object):
    def __init__(self,
                 block_num: int,
                 num_transactions: int,
                 logs: List[LogsInfo],
                 time_stamp: int,
                 transactions_value: decimal.Decimal,
                 block_reward_value: decimal.Decimal,
                 uncles_rewards_value: decimal.Decimal):
        self.block_num: int = block_num
        self.num_transactions: int = num_transactions
        self.logs = logs
        self.time_stamp = time_stamp
        self.transactions_value = transactions_value
        self.block_reward_value: decimal.Decimal = block_reward_value
        self.uncles_rewards_value: decimal.Decimal = uncles_rewards_value
        self.total_value: decimal.Decimal = transactions_value + block_reward_value + uncles_rewards_value

    def __repr__(self):
        return "BlockInfo: {}, logs_len: {}".format(self.block_num, len(self.logs))

    def to_json(self) -> Dict:
        return {
            'block_num': self.block_num,
            'num_transactions': self.num_transactions,
            'logs': [log.to_json() for log in self.logs],
            'time_stamp': self.time_stamp,
            'transactions_value': float(self.transactions_value),
            'block_reward_value': float(self.block_reward_value),
            'uncles_rewards_value': float(self.uncles_rewards_value),
            'total_value': float(self.total_value)
        }


class BlocksInfoEncoder(json.JSONEncoder):
    def default(self, o):
        return {'__{}__'.format(o.__class__.__name__): o.__dict__}
