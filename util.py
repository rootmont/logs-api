import functools
import logging
import json
from time import sleep
from typing import Union, Callable, List


def retry(method: Union[Callable, None] = None, retries: int = 5):
    """
    Retries a method if it throws
    :param method: method to wrap
    :param retries: number of retries to try
    :return: wrapped method that retries
    """
    # This recursion will wrap the argument positionally if it is entered.
    if method is None:
        return functools.partial(retry, retries=retries)

    @functools.wraps(method)
    def f(*args, **kwargs):
        ret = None
        for j in range(0, retries):
            try:
                # back off
                sleep(j)
                ret = method(*args, **kwargs)
                # we didn't throw so we're done
                break
            except Exception as e:
                if j == retries - 1:
                    raise e
                else:
                    logging.warning(e)
        return ret
    return f

